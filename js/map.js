var map = L.map('map').setView([55.444269617859504, 28.85490374645379], 8);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


var oblast_contur = '././custom.geo.json'
var points = '././points.json'
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var modal_content = document.getElementById("modal_content");
var point_title = document.getElementsByClassName("point_title")[0];

const GeoJsonData = await read_json(oblast_contur)
L.geoJson(GeoJsonData).addTo(map)

const PointsJsonData = await read_json(points)



function read_json(path) {
  return fetch(path)
    .then(response => response.json())
}

function markerOnClick(e) {
  modal.style.display = "block";
  modal_content.innerHTML = `${this.getLatLng().lat} - ${this.getLatLng().lng}`;

  for (let i = 0; i < PointsJsonData["points"].length; i++) {
    if (`${PointsJsonData["points"][i]["coords"]}` == `${this.getLatLng().lat},${this.getLatLng().lng}`) {
      modal_content.innerText = PointsJsonData["points"][i]["content"]
      point_title.innerText = PointsJsonData["points"][i]["title"]
    }
  }

}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


for (let i = 0; i < PointsJsonData["points"].length; i++) {
  var marker = L.marker(PointsJsonData["points"][i]["coords"]).on('click', markerOnClick).addTo(map);
}